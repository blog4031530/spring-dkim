### 선행 작업
샘플 코드를 실행하기 이전에 아래 개인키/공개키 파일을 생성해야 한다. 이를 위해서는 openssl CLI가 설치되어 있어야 한다.

### 키 파일 생성
다음과 같이 src/main/resources/keys 디렉토리에 파일을 생성해야 한다.
* rsa.private (PKCS1 개인키)
* pkcs8.rsa.private (PKCS8 개인키)
* rsa.public (공개키)
```
rsa.private, pkcs8.rsa.private 파일을 둘 중 하나만 생성하여 사용하면 되지만 rsa.private 파일을 사용하는 경우에는 RSAKeyLoader.loadPrivateKeyPKCS1()을 사용해야 한다.
```

```
cd src/main/resources
mkdir keys
cd keys

# PKCS1 개인키 파일 생성
openssl genrsa -out rsa.private 1024

# 공개키 파일 생성
$> openssl rsa -in rsa.private -out rsa.public -pubout -outform PEM

# PKCS1 -> PKCS8 로 변환
openssl pkey -in rsa.private -out pkcs8.rsa.private
```

### 공개키 데이터 치환
```
com.example.spring.dkim.verify.PublicKeyRecordRetriever4Test 파일에 <공개키데이터> 부분을 rsa.public 파일의 공개키 데이터로 치환해야 한다.
```