package com.example.spring.dkim;

import com.example.spring.dkim.sign.JamesDKIMSign;
import com.example.spring.dkim.sign.SimpleJavaDKIMSign;
import com.example.spring.dkim.verify.JamesDKIMVerify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;

import java.nio.file.Path;
import java.util.Optional;

@SpringBootApplication
@Slf4j
public class SpringDkimApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringDkimApplication.class, args);
	}

	@Override
	public void run(String... args) {
		//Apache james 라이브러리를 사용하여 DKIM-Signature 헤더를 생성한다.
		Path jamesOutputPath = jamesSign();
		Optional.ofNullable(jamesOutputPath).ifPresent(this::verify);

		//SimpleJavaMail 라이브러리를 사용하여 DKIM-Signature 헤더를 생성한다.
		Path simpleJavaMailoutputPath = simpleJavaMailSign();
		Optional.ofNullable(simpleJavaMailoutputPath).ifPresent(this::verify);
	}

	//target/classes/sample.eml 파일에 대한 DKIM-Signature 헤더를 생성하여
	//DKIM-Signature 헤더가 삽입된 target/classes/sample-james-dkim.eml 파일을 생성한다.
	Path jamesSign() {
		try {
			JamesDKIMSign jamesDKIMSign = new JamesDKIMSign();
			ClassPathResource resource = new ClassPathResource("sample.eml");
			Path originEmlPath = resource.getFile().toPath();
			Path outputEmlPath = originEmlPath.resolveSibling("sample-james-dkim.eml");
			jamesDKIMSign.makeDkimSignMessage(originEmlPath, outputEmlPath);
			return outputEmlPath;
		} catch (Exception e) {
			log.error("fail to dkim sign by james library, {}", e.getMessage());
			return null;
		}
	}

	//target/classes/sample.eml 파일에 대한 DKIM-Signature 헤더를 생성하여
	//DKIM-Signature 헤더가 삽입된 target/classes/sample-simple-javamail-dkim.eml 파일을 생성한다.
	Path simpleJavaMailSign() {
		try {
			SimpleJavaDKIMSign simpleJavaDKIMSign = new SimpleJavaDKIMSign();
			ClassPathResource resource = new ClassPathResource("sample.eml");
			Path originEmlPath = resource.getFile().toPath();
			Path outputEmlPath = originEmlPath.resolveSibling("sample-simple-javamail-dkim.eml");
			simpleJavaDKIMSign.makeDkimSignMessage(originEmlPath, outputEmlPath);
			return outputEmlPath;
		} catch (Exception e) {
			log.error("fail to dkim sign by simple java mail library, {}", e.getMessage());
			return null;
		}
	}

	/**
	 * apache james jdkim 라이브러리를 사용하여 파라미터로 전달된 경로의 EML 에 대한 DKIM 검증을 수행한다.
	 * @param emlPath DKIM-Signature 헤더가 삽입된 eml 파일 경로
	 */
	void verify(Path emlPath) {
		try {
			JamesDKIMVerify jamesDKIMVerify = new JamesDKIMVerify();
			jamesDKIMVerify.verify(emlPath);
		} catch (Exception e) {
			log.error("fail to verify, {}", e.getMessage());
		}
	}
}
