package com.example.spring.dkim.sign;

import com.example.spring.dkim.common.RSAKeyLoader;
import jakarta.mail.MessagingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.james.jdkim.DKIMSigner;
import org.apache.james.jdkim.exceptions.FailException;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;

@Slf4j
public class JamesDKIMSign {
	public JamesDKIMSign() {}
	public void makeDkimSignMessage(Path originEml, Path outputEml) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, FailException, MessagingException {
		//DKIM-Signature header tag 셋팅
		String signatureTemplate =
			"v=1; a=rsa-sha256; s=selector; c=simple/simple; d=test.com; " +
			"h=Content-Type:MIME-Version:Subject:Message-ID:To:Sender:From:Date; bh=; b=";

		//PKCS8 개인키를 로딩한다.
		ClassPathResource privateKeyResource = new ClassPathResource("keys/pkcs8.rsa.private");
		PrivateKey privateKey = RSAKeyLoader.loadPrivateKeyPKCS8(privateKeyResource.getFile().toPath());
		//PKCS1 개인키를 로딩하려면 아래 메서드를 호출한다.
//		ClassPathResource privateKeyResource = new ClassPathResource("keys/rsa.private");
//		PrivateKey privateKey = RSAKeyLoader.loadPrivateKeyPKCS1(privateKeyResource.getFile().toPath());

		DKIMSigner signer = new DKIMSigner(signatureTemplate, privateKey);
		String sign;
		try (InputStream inputStream = Files.newInputStream(originEml)) {
			//sign() 메서드 호출을 통해서 DKIM-Signature 헤더를 얻을 수 있다.
			//sign() 호출 이후 inputStream은 close 된다.
			sign = signer.sign(inputStream);
			log.info("{}", sign);
			sign += "\r\n";
		}

		//outputEml 에 DKIM-Signature 헤더를 추가하여 originEml을 복사한다.
		try (InputStream inputStream = Files.newInputStream(originEml);
			OutputStream outputStream = Files.newOutputStream(outputEml)) {
			outputStream.write(sign.getBytes(Charset.defaultCharset()));
			outputStream.write(inputStream.readAllBytes());
		}
	}
}
