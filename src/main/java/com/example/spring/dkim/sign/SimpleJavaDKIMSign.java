package com.example.spring.dkim.sign;

import com.example.spring.dkim.common.RSAKeyLoader;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.simplejavamail.utils.mail.dkim.Canonicalization;
import org.simplejavamail.utils.mail.dkim.DkimMessage;
import org.simplejavamail.utils.mail.dkim.DkimSigner;
import org.simplejavamail.utils.mail.dkim.SigningAlgorithm;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;

public class SimpleJavaDKIMSign {

	/**
	 * @param originEml DKIM 서명하고자 하는 EML 경로
	 * @param outputEml DKIM 서명 헤더를 붙인 새로운 EML 경로
	 */
	public void makeDkimSignMessage(Path originEml, Path outputEml) throws IOException, MessagingException, InvalidKeySpecException, NoSuchAlgorithmException {
		try (InputStream inputStream = Files.newInputStream(originEml);
			 OutputStream outputStream = Files.newOutputStream(outputEml)) {
			MimeMessage originMessage = new MimeMessage(null, inputStream);
			DkimMessage dkimMessage = getDkimMessage(originMessage);
			dkimMessage.writeTo(outputStream);
		}

		//check exists DKIM-Signature header
		try (InputStream inputStream = Files.newInputStream(outputEml)) {
			MimeMessage outputMessage = new MimeMessage(null, inputStream);
			String[] header = outputMessage.getHeader("DKIM-Signature");
			assert header.length > 0;
		}
	}

	private DkimMessage getDkimMessage(MimeMessage message) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException, MessagingException {
		final String domain = "test.com";
		final String selector = "selector";
		ClassPathResource privateKeyResource = new ClassPathResource("keys/pkcs8.rsa.private");
		RSAPrivateKey privateKey = RSAKeyLoader.loadPrivateKeyPKCS8(privateKeyResource.getFile().toPath());

		DkimSigner dkimSigner = new DkimSigner(domain, selector, privateKey);
		dkimSigner.setHeaderCanonicalization(Canonicalization.SIMPLE);
		dkimSigner.setBodyCanonicalization(Canonicalization.SIMPLE);
		dkimSigner.setSigningAlgorithm(SigningAlgorithm.SHA256_WITH_RSA);

		//지정된 도메인 및 지정된 selector에 대한 DNS 리소스 레코드가 올바르게 준비되었는지 확인하는 메서드다.
		//테스트를 위해서 실제로 DNS 구성을 하지 않았기 때문에 false로 지정한다.
		//리얼 환경에서는 아래 코드는 지정하지 않는 것이 좋다.
		dkimSigner.setCheckDomainKey(false);
		return new DkimMessage(message, dkimSigner);
	}
}
