package com.example.spring.dkim.verify;

import lombok.extern.slf4j.Slf4j;
import org.apache.james.jdkim.DKIMVerifier;
import org.apache.james.jdkim.api.PublicKeyRecordRetriever;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.exceptions.FailException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Slf4j
public class JamesDKIMVerify {
	public JamesDKIMVerify() {}

	public void verify(Path emlPath) throws IOException, FailException {
		PublicKeyRecordRetriever publicKeyRecordRetriever = new PublicKeyRecordRetriever4Test();
		try(InputStream inputStream = Files.newInputStream(emlPath)) {
			DKIMVerifier verifier = new DKIMVerifier(publicKeyRecordRetriever);
			List<SignatureRecord> verify = verifier.verify(inputStream);
			verify.forEach(signatureRecord -> log.info("signature record: {}", signatureRecord));
		}
	}
}
