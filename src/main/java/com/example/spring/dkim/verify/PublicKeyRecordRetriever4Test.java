package com.example.spring.dkim.verify;

import org.apache.james.jdkim.api.PublicKeyRecordRetriever;

import java.util.List;

/**
 * PublicKeyRecordRetriever 클래스는 apache james의 jdkim 라이브러리에서 제공하는 인터페이스다.
 * 서명 검증시 내부적으로 DKIM-Signature 헤더의 selector, domain 정보를 기반으로 DNS 조회를 통해서 공개키를 가져오는데
 * DNS 조회를 통해서 공개키를 가져오는 역할은 PublicKeyRecordRetriever 구현체가 담당한다.
 * 검증 테스트를 위해서 DNS에 공개키를 등록하지 않았기 때문에 하드코딩된 공개키를 가져오도록 한다.
 * PublicKeyRecordRetriever4Test 클래스는 하드코딩된 공개키 정보를 가져오도록 하기 위한 장치다.
 * p= 태그에 BEGIN PUBLIC KEY, END PUBLIC KEY 라인을 제외한
 * resource/keys/rsa.public 파일의 공개키 내용을 직접 셋팅하도록 한다.
 */
public class PublicKeyRecordRetriever4Test implements PublicKeyRecordRetriever {
	@Override
	public List<String> getRecords(CharSequence methodAndOption,
								   CharSequence selector,
								   CharSequence token) {
		//TODO <공개키데이터>를 rsa.public 파일의 데이터로 치환해야 한다.
		//example
		//String publicKeyRecord = "v=DKIM1; k=rsa; " +
		//	"p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDjm1g0xmCM8eCveYP70SAdRNa1" +
		//	"2y0absnxuOcZy/hvSyQqNqS5hoFcfvn/tQCO4LrcccLoRSueMOOc6l0rHpRcHluX++t" +
		//	"FMbG7IkxcMT1Rk/vnBQ31GeTfeEbR4y/hBMdU5DDvRsvkSXXVlc9Us7m8te6hPFQYxtCEgJx1MhQuoQIDAQAB";
		String publicKeyRecord = "v=DKIM1; k=rsa; " +
			"p=<공개키데이터>";
		return List.of(publicKeyRecord);
	}
}
